///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   Years: 10 Days: 357 Hours: 8 Minutes: 8 Seconds: 4
//   Years: 10 Days: 357 Hours: 8 Minutes: 8 Seconds: 5
//   Years: 10 Days: 357 Hours: 8 Minutes: 8 Seconds: 6
//   Years: 10 Days: 357 Hours: 8 Minutes: 8 Seconds: 7
//   Years: 10 Days: 357 Hours: 8 Minutes: 8 Seconds: 8
//
//
//
// @author Christian Li <lichrist@hawaii.edu>
// @date   22_Feb_2022
///////////////////////////////////////////////////////////////////////////////
//
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <unistd.h>
#include <stdbool.h>

int main() {

   time_t timeRef;

   int years;
   int days; 
   int hours;
   int minutes;
   int seconds;
   int counts;
   
   struct tm reference_time;
   reference_time = *localtime(&timeRef);

   reference_time.tm_year = 111;
   reference_time.tm_mon = 2;
   reference_time.tm_mday = 5;
   reference_time.tm_hour = 15;
   reference_time.tm_min = 0;
   reference_time.tm_sec = 0;

   printf("Reference Time (HST): %s\n", asctime(&reference_time));

   while(true){
      time(&timeRef);
      mktime(&reference_time);
      
      counts = difftime(timeRef, mktime(&reference_time));

      years = counts / (31536000);
      days = counts % (31536000) / (86400);
      hours = counts % (86400) / (3600);
      minutes = counts % (3600) / (60);
      seconds = counts % (60);
      
      printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n", years, days, hours, minutes, seconds);

      sleep(1);
   }
   return 0;

}
